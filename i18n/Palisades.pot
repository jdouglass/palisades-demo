# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-14 13:50-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: palisades/__init__.py:81
#, python-format
msgid "Configuration file %s could not be found in %s"
msgstr ""

#: palisades/__init__.py:87
#, python-format
msgid "Showing splash %s"
msgstr ""

#: palisades/__init__.py:89
msgid "Building core application"
msgstr ""

#: palisades/__init__.py:93
#, python-format
msgid "Setting runner class to %s"
msgstr ""

#: palisades/__init__.py:97
msgid "Building graphical interface"
msgstr ""

#: palisades/__init__.py:101
msgid "Starting application"
msgstr ""

#: palisades/elements.py:395 palisades/gui/core.py:327
msgid "Element is required"
msgstr ""

#: palisades/gui/core.py:41
msgid "Starting ..."
msgstr ""

#: palisades/gui/core.py:65
msgid "Ready!"
msgstr ""

#: palisades/gui/core.py:331
msgid "(Element is optional)"
msgstr ""

#: palisades/gui/qt4.py:477
msgid "WARNING:"
msgstr ""

#: palisades/gui/qt4.py:480
msgid "ERROR:"
msgstr ""

#: palisades/gui/qt4.py:483
msgid "Validation successful"
msgstr ""

#: palisades/gui/qt4.py:598
msgid "Refresh"
msgstr ""

#: palisades/gui/qt4.py:603
msgid "Reset to default"
msgstr ""

#: palisades/gui/qt4.py:708 palisades/gui/qt4.py:709
msgid "All files (* *.*)"
msgstr ""

#: palisades/gui/qt4.py:710
msgid "Comma separated value file (*.csv *.CSV)"
msgstr ""

#: palisades/gui/qt4.py:711
msgid "[GDAL] Arc/Info Binary Grid (hdr.adf HDR.ADF hdr.ADF)"
msgstr ""

#: palisades/gui/qt4.py:712
msgid "[GDAL] Arc/Info ASCII Grid (*.asc *.ASC)"
msgstr ""

#: palisades/gui/qt4.py:713
msgid "[GDAL] GeoTiff (*.tif *.tiff *.TIF *.TIFF)"
msgstr ""

#: palisades/gui/qt4.py:714
msgid "[OGR] ESRI Shapefiles (*.shp *.SHP)"
msgstr ""

#: palisades/gui/qt4.py:715
msgid "[DBF] dBase legacy file (*dbf *.DBF)"
msgstr ""

#: palisades/gui/qt4.py:728 palisades/gui/qt4.py:745
msgid "Select "
msgstr ""

#: palisades/gui/qt4.py:783
msgid "Errors exist!"
msgstr ""

#: palisades/gui/qt4.py:791 palisades/gui/qt4.py:856
msgid "Whoops!"
msgstr ""

#: palisades/gui/qt4.py:795
msgid "OK"
msgstr ""

#: palisades/gui/qt4.py:836
msgid "Warning..."
msgstr ""

#: palisades/gui/qt4.py:838
msgid ""
"Some inputs cannot be validated and may cause this program to fail.  "
"Continue anyways?"
msgstr ""

#: palisades/gui/qt4.py:840
msgid "Back"
msgstr ""

#: palisades/gui/qt4.py:847
msgid "Are you sure you want to quit?"
msgstr ""

#: palisades/gui/qt4.py:848
msgid "Really quit?"
msgstr ""

#: palisades/gui/qt4.py:850
msgid "You will lose any changes to your parameter fields."
msgstr ""

#: palisades/gui/qt4.py:851
msgid "Quit"
msgstr ""

#: palisades/gui/qt4.py:866
msgid "is 1 error"
msgstr ""

#: palisades/gui/qt4.py:868
#, python-format
msgid "are %s errors"
msgstr ""

#: palisades/gui/qt4.py:870
#, python-format
msgid "There %s that must be resolved"
msgstr ""

#: palisades/gui/qt4.py:871
#, python-format
msgid " before this tool can be run:%s"
msgstr ""

#: palisades/gui/qt4.py:886
#, python-format
msgid "Tab %s"
msgstr ""

#: palisades/gui/qt4.py:948
msgid "Running the model"
msgstr ""

#: palisades/gui/qt4.py:956
msgid "Messages:"
msgstr ""

#: palisades/gui/qt4.py:982
msgid " Back"
msgstr ""

#: palisades/gui/qt4.py:983
msgid "Return to parameter list"
msgstr ""

#: palisades/gui/qt4.py:1009
msgid "Initializing...\n"
msgstr ""

#: palisades/gui/qt4.py:1046
msgid "See the log for details."
msgstr ""

#: palisades/gui/qt4.py:1049
msgid "Model completed successfully."
msgstr ""

#: palisades/gui/qt4.py:1086
msgid "&File"
msgstr ""

#: palisades/gui/qt4.py:1087
msgid "&Load parameters from file ..."
msgstr ""

#: palisades/gui/qt4.py:1089
msgid "&Save parameters ..."
msgstr ""

#: palisades/gui/qt4.py:1092
msgid "Exit"
msgstr ""

#: palisades/gui/qt4.py:1096
msgid "&Development"
msgstr ""

#: palisades/gui/qt4.py:1097
msgid "Save to &python script..."
msgstr ""

#: palisades/gui/qt4.py:1126
msgid " Run"
msgstr ""

#: palisades/gui/qt4.py:1129
msgid " Quit"
msgstr ""
